<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db;       // = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
            
			try{
				$this->db = new PDO('mysql:host=localhost;dbname=book','root','');
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}catch(PDOException $x){
				echo "Error: ".$x->getMessage();
			
			}




		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		
		$booklist = array();
		$stmt = $this->db->query('SELECT * FROM Book ORDER BY id');
		while ($row = $stmt->fetch (PDO::FETCH_ASSOC)){
			$booklist[] = new Book($row ['Title'], $row ['Author'], $row ['Description'], $row ['ID']);
		}
        return $booklist;
		
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		$book = NULL;
		try{
			$stmt = $this->db->prepare('SELECT * FROM book WHERE id=:id');
			$stmt->bindValue(':id', $id, PDO::PARAM_INT);
			if($id > 0 && is_numeric($id)){
				$stmt->execute();
				$rows = $stmt->fetch(PDO::FETCH_ASSOC);
				if($rows){
					$book = new Book($rows['Title'], $rows['Author'], $rows['Description'], $rows['ID']);
					return $book;
				}
				else{return NULL;
				}
			}//else{return true;}
		}catch(PDOException $ex){
			$view = new ErrorView();
			$view->create();
		echo "No book ".$ex->getMessage();
		}
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		try{
		$stmt = $this->db->prepare("INSERT INTO book (Title, Author, Description) VALUES(:t, :a, :d)");
		$stmt->bindValue(':t', $book->title, PDO::PARAM_STR);
		$stmt->bindValue(':a', $book->author, PDO::PARAM_STR);
		$stmt->bindValue(':d', $book->description, PDO::PARAM_STR);
		if(!empty($book->title) && !empty($book->author)){
			$stmt->execute();
		} else{
			$view = new ErrorView();
			$view->create();
			echo "You must fill the title- and author- fields";
		}
		
		}catch(Exception $x){
			$view = new ErrorView();
			$view->create();
			echo "error!!!";$x->getMessage();
		}
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {  
	   try{
			$stmt = $this->db->prepare("UPDATE book SET Title =:title , Author = :author,Description=:description WHERE id=:id");
			$stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
			$stmt->bindValue(':author', $book->author, PDO::PARAM_STR);
			$stmt->bindValue(':description', $book->description, PDO::PARAM_STR);
			$stmt->bindValue(':id',$book->id, PDO::PARAM_INT);
		if(!empty($book->title) && !empty($book->author)){
			$stmt->execute();
		} else{
			$view = new ErrorView();
		    $view->create();
			echo "You must fill the title- and author- fields";
		}
	   }
	   catch(Exception $x){
		   $view = new ErrorView();
		   $view->create();
		   echo 'error!!!';$x->getMessage();   
	   }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		try{
			$stmt = $this->db->prepare("DELETE FROM book WHERE id=:id");
			$stmt->bindValue(':id',$id,PDO::PARAM_INT);
			$stmt->execute();
	}
	catch(Exception $x){
		$view = new ErrorView();
		$view->create();
		echo 'error!!!';$x->getMessage();
	}
    }
	
}

?>